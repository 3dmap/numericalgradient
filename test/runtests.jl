using Test
include("../src/NumericalGradient.jl")

@testset "Array gradient" begin
    v = vec([1 3 2 5 4 6])
    result = NumericalGradient.gradient(v)
    @test result == vec([2.0 0.5 1.0 1.0 0.5 2.0])

    @test_throws MethodError NumericalGradient.gradient([])

    @test NumericalGradient.gradient(vec([1])) == vec([0])
end


@testset "Matrix gradient" begin
    m = [1 2 3; 4 5 6; 7 8 9]
    dx, dy = NumericalGradient.gradient(m)
    @test dx == ones(size(dx))
    @test dy == fill(3, size(dy))

    m = [1 2 3]
    dx, dy = NumericalGradient.gradient(m)
    @test dx == ones(size(dx))
    @test dy == zeros(size(dy))

    m = [1 3 9; 6 2 7; 6 9 4]
    dx, dy = NumericalGradient.gradient(m)
    @test dx == [2 4 6; -4 .5 5; 3 -1 -5]
    @test dy == [5 -1 -2; 2.5 3 -2.5; 0 7 -3]
end