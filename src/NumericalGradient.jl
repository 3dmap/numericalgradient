module NumericalGradient

export gradient, gradient!

function gradient(m::AbstractMatrix{<:Number})
    return gradient(m, axes(m, 2), axes(m, 1))
end

"""
    gradient!(dx, dy, m, hx, hy)

Compute the numerical gradient of the matrix m with step hx and hy and return the result a tuple of result

# Examples
```Julia
m = [ 1 2 3 ; 4 5 6 ; 7 8 9 ]
hx = axis(m, 2)
hy = axis(m, 1)

dx, dy = gradient( m, hx, hy)

println(dx)
# 3×3 Matrix{Float64}:
# 1.0  1.0  1.0
# 1.0  1.0  1.0
# 1.0  1.0  1.0
```

"""
function gradient(m::AbstractMatrix{<:Number}, hx::AbstractArray{<:Number}, hy::AbstractArray{<:Number})
    dx = zeros(size(m))
    dy = zeros(size(m))
    return gradient!(dx, dy, m, hx, hy)
end

function gradient(v::AbstractArray{<:Number})
    return gradient(v, axes(v, 1))
end

function gradient(v::AbstractVector{<:Number}, hx::AbstractArray{<:Number})
    dx = zeros(size(v))
    return gradient!(dx, v, hx)
end

function gradient!(dx::AbstractArray{Float64}, v::AbstractArray{<:Number}, hx::AbstractArray{<:Number})
    if length(v) == 0
        throw("Invalid array of length 0") 
    elseif length(v) >= 3
        dx[2:end-1] = (v[3:end] - v[1:end-2]) ./ (hx[3:end] - hx[1:end-2])
    end
    if length(v) >= 2
        dx[1] = (v[2] - v[1]) ./ (hx[2] - hx[1])
        dx[end] = (v[end] - v[end-1]) ./ (hx[end] - hx[end-1])
    else
        dx[1] = 0
        dx[end] = 0
    end
    return dx
end

"""
    gradient!(dx, dy, m, hx, hy)

Compute the numerical gradient of the matrix m with step hx and hy and store the result in dx and dy

# Examples
```Julia
m = [ 1 2 3 ; 4 5 6 ; 7 8 9 ]
dx = zeros(size(m))
dy = zeros(size(m))
hx = axis(m, 2)
hy = axis(m, 1)

gradient!(dx, dy, m, hx, hy)

println(dx)
# 3×3 Matrix{Float64}:
# 1.0  1.0  1.0
# 1.0  1.0  1.0
# 1.0  1.0  1.0
```

"""
function gradient!(dx::AbstractMatrix{Float64}, dy::AbstractMatrix{Float64}, m::AbstractMatrix{<:Number}, hx::AbstractArray{<:Number}, hy::AbstractArray{<:Number})
    if size(m, 2) >= 3
        dx[:, 2:end-1] = (m[:, 3:end] - m[:, 1:end-2]) ./ (hx[3:end] - hx[1:end-2])'
    end
    if size(m, 2) >= 2
        dx[:, 1] = (m[:, 2] - m[:, 1]) ./ (hx[2] - hx[1])
        dx[:, end] = (m[:, end] - m[:, end-1]) ./ (hx[end] - hx[end-1])
    else
        dx[:, 1] .= 0
        dx[:, end] .= 0
    end
    if size(m, 1) >= 3
        dy[2:end-1, :] = (m[3:end, :] - m[1:end-2, :]) ./ (hy[3:end] - hy[1:end-2])
    end
    if size(m, 1) >= 2
        dy[1, :] = (m[2, :] - m[1, :]) ./ (hy[2] - hy[1])
        dy[end, :] = (m[end, :] - m[end-1, :]) ./ (hy[end] - hy[end-1])
    else
        dy[1, :] .= 0
        dy[end, :] .= 0
    end
    return dx, dy
end
end